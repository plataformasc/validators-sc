/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateBoolean
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Interfaz de la anotacion ValidateBoolean
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Valida atributos o parametros de tipo <code>Boolean</code> Es invocada
 * anotando un atributo con <code> @ValidateBoolean </code> y puede recibir los
 * siguientes parametros
 * <code>String message, NumberConstraintRule ruleValue, boolean value</code>
 * <p>
 * Donde:
 * <p>
 * </code>message</code> es el mensaje de retorno cuando la validacion falla.
 * </p>
 * <p>
 * </code>value</code> es el valor con el que se compara el dato anotado.
 * <p>
 * <code>ruleValue</code> es la regla que se utilizara para ejecutar la
 * validacion
 * <p>
 * Ejemplo:
 * <code> @ValidateBoolean (ruleValue=NumberConstraintRule.FALSE, message="Dato Invalido")
 * boolean data;
 * </code>
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateBooleanEngine, NumberConstraintRule
 */
@Constraint(validatedBy = { ValidateBooleanEngine.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateBoolean {
    String message() default "El tipo Boolean no es valido";

    NumberConstraintCode ruleValue() default NumberConstraintCode.NOT_NULL;

    boolean value() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
