/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidatePasswordEngine
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que implementa la anotacion ValidatePassword
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementa la logica de funcionamiento para la anotacion
 * <code>@ValidatePassword</code> Determina si el atributo o parametro que fue
 * anotado es valido, utilizando los argumentos que acompanan a la anotacion
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidatePassword
 */
public class ValidatePasswordEngine implements ConstraintValidator<ValidatePassword, String> {
    Integer definedStrenght;

    @Override
    public void initialize(ValidatePassword constraintAnnotation) {
        this.definedStrenght = constraintAnnotation.passwordStrength();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null ? false : definedStrenght <= PasswordStrengthCalculator.calculatePasswordStrength(value);
    }
}
