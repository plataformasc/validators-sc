/**
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleFactory
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator.rule;

/**
 * Genera instancias de las reglas de validaciones mediante métodos factories.
 *
 * @author j49u4r
 *
 */
public final class RuleFactory {

    private RuleFactory() {
    }

    /**
     * Devuelve una instacia de la regla de validación de longitud de un
     * password.
     *
     * @param points
     *            Puntos asignados al password si cumple esta regla.
     * @return Regla de validación de longitud de un password.
     */
    public static RuleExtended ofLengthRule(int points) {
	return new RuleLength(points);
    }

    /**
     * Devuelve una instacia de la regla de validación de longitud de un
     * password.
     *
     * @param points
     *            Puntos asignados al password si cumple esta regla.
     * @param minLength
     *            Longitud mínima aceptable para el password.
     * @param maxLength
     *            Longitud máxima aceptable para el password.
     * @return Regla de validación de longitud de un password.
     */
    public static RuleExtended ofLLengthRuleMinMax(int points, int minLength, int maxLength) {
	return new RuleLength(points, minLength, maxLength);
    }

    /**
     * Devuelve una instancia de la regla que valida si un password contiene al
     * menos una letra mayúscula.
     *
     * @param paramPoints
     *            Puntos asignados al password si cumple esta regla.
     * @return Regla de validación de letras mayúscula.
     */
    public static RuleExtended ofRuleUpperCase(int paramPoints) {
	return new RuleUpperCase(paramPoints);
    }
}
