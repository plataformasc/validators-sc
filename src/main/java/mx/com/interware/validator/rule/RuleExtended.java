/**
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleExtended
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:     
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator.rule;

import edu.vt.middleware.password.Password;
import edu.vt.middleware.password.PasswordData;
import edu.vt.middleware.password.Rule;

/**
 * Define los atributos que posee una regla de validación. En este caso son la
 * regla misma como tal y los puntos asignados a dicha regla cuando se calcule
 * la fortaleza del password. Si una cadena cumple con la regla definida,
 * entonces se devuelve el total de puntos asignados a la regla y cero puntos en
 * caso contrario.
 * 
 * @author j49u4r
 *
 */
public abstract class RuleExtended {

    private Rule rule;
    private int points;

    public void setRule(Rule paramRule) {
	rule = paramRule;
    }

    public void setPoints(int paramPoints) {
	points = paramPoints;
    }

    /**
     * Permite ejecutar la regla de validación, y en caso de ser aprobada se
     * devuelve el total de puntos que posee la regla. Si no se aprueba la
     * regla, entonces se devuelve un total de cero puntos.
     * 
     * @param paramPassword
     *            Cadena como password sobre la que se ejecuta la regla.
     * @return Los puntos en caso de aprobar la regla; cero en caso de no
     *         aprobar la regla.
     */
    public int validate(String paramPassword) {
	Password password = new Password(paramPassword);
	PasswordData passwordData = new PasswordData(password);
	return rule.validate(passwordData).isValid() ? points : 0;
    }
}
