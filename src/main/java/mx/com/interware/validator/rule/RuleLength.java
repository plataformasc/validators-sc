/**
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleLength
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:     
 * 
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator.rule;

import edu.vt.middleware.password.LengthRule;

/**
 * Define una regla de validación de longitud de cadena, indicando la cantidad
 * de puntos asignados a esta regla.
 * 
 * @author j49u4r
 *
 */
public class RuleLength extends RuleExtended {

    private static final int DEFAULT_MIN_LENGTH = 4;
    private static final int DEFAULT_MAX_LENGTH = 30;

    /**
     * Genera una instancia de la regla usando el mínimo y el máximo de longitud
     * por default.
     * 
     * @param points
     *            Puntos a asignar a la regla de longitud.
     */
    protected RuleLength(int points) {
	setRule(new LengthRule(DEFAULT_MIN_LENGTH, DEFAULT_MAX_LENGTH));
	setPoints(points);
    }

    /**
     * Genera una instancia de la regla usando el mínimo y el máximo de longitud
     * definidos por el usuario.
     * 
     * @param points
     *            Puntos a asignar a la regla de longitud.
     */
    protected RuleLength(int points, int minLength, int maxLength) {
	setRule(new LengthRule(minLength, maxLength));
	setPoints(points);
    }
}
