/**
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleLowerCase
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator.rule;

/**
 * Verifica que una cadena considerada como password contenga al menos una
 * minúscula.
 *
 * @author j49u4r
 *
 */
public class RuleLowerCase extends RuleExtended {

}
