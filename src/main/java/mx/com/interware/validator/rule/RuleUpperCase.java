/**
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleUpperCase
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator.rule;

import edu.vt.middleware.password.UppercaseCharacterRule;

/**
 * Define el constructor de una regla de validación de letras mayúsculas en una
 * cadena que representa el password.
 *
 * @author j49u4r
 *
 */
public class RuleUpperCase extends RuleExtended {

    /**
     * Cantidad de letras mayúsculas que debe contener un password por defecto.
     */
    private static final int DEFAULT_AMOUNT_OF_UPPER = 3;

    /**
     * Regla para determinar si un password tiene 3 caracteres en mayúsculas.
     *
     * @param points
     *            Puntos asignados a la regla de mayúsculas.
     */
    protected RuleUpperCase(int points) {
	this.setRule(new UppercaseCharacterRule(DEFAULT_AMOUNT_OF_UPPER));
	this.setPoints(points);
    }

    protected RuleUpperCase(int points, int amountOfUpper) {
	this.setRule(new UppercaseCharacterRule(amountOfUpper));
	this.setPoints(points);
    }
}
