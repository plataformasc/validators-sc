/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateDateEngine
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que implementa la anotacion ValidateDate
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementa la logica de funcionamiento para la anotacion
 * <code>@ValidateDate</code> Determina si el atributo o parametro que fue
 * anotado es valido, utilizando los argumentos que acompanan a la anotacion
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateDate, NumberConstraintRule
 */
public class ValidateDateEngine implements ConstraintValidator<ValidateDate, Date> {
    private DateFormat formatter;
    private String dateFormat;
    private NumberConstraintCode ruleValue;
    private String axisValueString;
    private String leftValueString;
    private String rightValueString;
    private Date axisValueDate;
    private Date leftValueDate;
    private Date rightValueDate;

    @Override
    public void initialize(ValidateDate validateDate) {
        this.axisValueString = validateDate.value();
        this.leftValueString = validateDate.leftValue();
        this.rightValueString = validateDate.rightValue();
        this.ruleValue = validateDate.ruleValue();
        this.dateFormat = validateDate.dateFormat();
        formatter = new SimpleDateFormat(dateFormat);
        parseArgumentsIntoDate();
    }

    @Override
    public boolean isValid(Date mainValue, ConstraintValidatorContext constraintValidatorContext) {
        switch (this.ruleValue) {
        case NULL:
            return (mainValue == null);
        case NOT_NULL:
            return (mainValue != null);
        case GREATER_THAN:
            return mainValue == null ? false : (axisValueDate == null ? false : mainValue.after(axisValueDate));
        case LESS_THAN:
            return mainValue == null ? false : (axisValueDate == null ? false : mainValue.before(axisValueDate));
        case EQUAL_TO:
            return mainValue == null ? false : (axisValueDate == null ? false : mainValue.equals(axisValueDate));
        case DIFFERENT_TO:
            return mainValue == null ? false : (axisValueDate == null ? false : !mainValue.equals(axisValueDate));
        case BETWEEN_EXCLUSIVE:
            return mainValue == null ? false
                    : (leftValueDate == null || rightValueDate == null ? false
                            : mainValue.after(leftValueDate) && mainValue.before(rightValueDate));
        case NOT_BETWEEN_EXCLUSIVE:
            return mainValue == null ? false
                    : (leftValueDate == null || rightValueDate == null ? false
                            : mainValue.before(leftValueDate) || mainValue.after(rightValueDate));
        default:
            return (false);
        }
    }

    private void parseArgumentsIntoDate() {
        try {
            axisValueDate = axisValueString != "" ? (Date) formatter.parse(axisValueString) : null;
            leftValueDate = leftValueString != "" ? (Date) formatter.parse(leftValueString) : null;
            rightValueDate = rightValueString != "" ? (Date) formatter.parse(rightValueString) : null;
        } catch (ParseException e) {
        }
    }
}
