/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateIntegerEngine
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que implementa la anotacion ValidateInteger
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementa la logica de funcionamiento para la anotacion
 * <code>@ValidateInteger</code> Determina si el atributo o parametro que fue
 * anotado es valido, utilizando los argumentos que acompanan a la anotacion
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateInteger, NumberConstraintRule
 */
public class ValidateIntegerEngine implements ConstraintValidator<ValidateInteger, Integer> {
    private NumberConstraintCode ruleValue;
    private Integer axisValue;
    private Integer leftValue;
    private Integer rightValue;

    @Override
    public void initialize(ValidateInteger validateInteger) {
        this.axisValue = validateInteger.value();
        this.leftValue = validateInteger.leftValue();
        this.rightValue = validateInteger.rightValue();
        this.ruleValue = validateInteger.ruleValue();
    }

    @Override
    public boolean isValid(Integer mainValue, ConstraintValidatorContext constraintValidatorContext) {
        switch (this.ruleValue) {
        case NULL:
            return (mainValue == null);
        case NOT_NULL:
            return (mainValue != null);
        case ZERO:
            return mainValue == null ? false : (mainValue == 0);
        case NOT_ZERO:
            return mainValue == null ? false : (mainValue != 0);
        case GREATER_THAN:
            return mainValue == null ? false : (mainValue > axisValue);
        case GREATER_THAN_OR_EQUAL_TO:
            return mainValue == null ? false : (mainValue >= axisValue);
        case LESS_THAN:
            return mainValue == null ? false : (mainValue < axisValue);
        case LESS_THAN_OR_EQUAL_TO:
            return mainValue == null ? false : (mainValue <= axisValue);
        case EQUAL_TO:
            return mainValue == null ? false : (mainValue == axisValue);
        case DIFFERENT_TO:
            return mainValue == null ? false : (mainValue != axisValue);
        case BETWEEN_EXCLUSIVE:
            return mainValue == null ? false : (leftValue < mainValue && mainValue < rightValue);
        case BETWEEN_INCLUSIVE:
            return mainValue == null ? false : (leftValue <= mainValue && mainValue <= rightValue);
        case NOT_BETWEEN_EXCLUSIVE:
            return mainValue == null ? false : (mainValue < leftValue || mainValue > rightValue);
        case NOT_BETWEEN_INCLUSIVE:
            return mainValue == null ? false : (mainValue <= leftValue || mainValue >= rightValue);
        case NEGATIVE:
            return mainValue == null ? false : (mainValue < 0);
        case POSITIVE:
            return mainValue == null ? false : (mainValue >= 0);
        default:
            return (false);
        }
    }
}
