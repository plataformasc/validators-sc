package mx.com.interware.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = { ValidateStringEngine.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateString {
    String message() default "El parametro es invalido";

    ValidatorCode value();

    String customRegEx() default "";

    int minLen() default 1;

    int maxLen() default Integer.MAX_VALUE;

    int strength() default 1;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
