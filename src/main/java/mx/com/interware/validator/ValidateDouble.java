/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateDouble
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Interfaz de la anotacion ValidateDouble
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Valida atributos o parametros de tipo <code>Double</code> Es invocada
 * anotando un atributo con <code> @ValidateDouble</code> y puede recibir los
 * siguientes parametros
 * <code>String message, NumberConstraintRule ruleValue, Double value, Double leftValue, Double rightValue</code>
 * <p>
 * Donde:
 * <p>
 * </code>message</code> es el mensaje de retorno cuando la validacion falla.
 * </p>
 * <p>
 * </code>value</code> es el valor con el que se compara el dato anotado.
 * <p>
 * <code>leftValue</code> y <code>rightValue</code> definen el rango de valores
 * en el que se ejecutan operaciones como
 * <code>NumberConstraintRule.BETWEEN_EXCLUSIVE</code>
 * <p>
 * <code>ruleValue</code> es la regla que se utilizara para ejecutar la
 * validacion
 * <p>
 * Ejemplo:
 * <code> @ValidateDouble (ruleValue=NumberConstraintRule.BETWEEN_EXCLUSIVE, value=10, leftValue=0, rightValue=40, message="Dato Invalido")
 * Double data;
 * </code>
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateDoubleEngine, NumberConstraintRule
 */
@Constraint(validatedBy = { ValidateDoubleEngine.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateDouble {
    String message() default "El tipo Double no es valido";

    NumberConstraintCode ruleValue() default NumberConstraintCode.ZERO;

    double value() default 0;

    double leftValue() default Double.MIN_VALUE;

    double rightValue() default Double.MAX_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
