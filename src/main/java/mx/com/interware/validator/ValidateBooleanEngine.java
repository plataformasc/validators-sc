/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateBooleanEngine
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que implementa de la anotacion ValidateBoolean
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Implementa la logica de funcionamiento para la anotacion
 * <code>@ValidateBoolean</code> Determina si el atributo o parametro que fue
 * anotado es valido, utilizando los argumentos que acompanan a la anotacion
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateBoolean, NumberConstraintRule
 */
public class ValidateBooleanEngine implements ConstraintValidator<ValidateBoolean, Boolean> {
    private NumberConstraintCode ruleValue;

    @Override
    public void initialize(ValidateBoolean validateBoolean) {
        this.ruleValue = validateBoolean.ruleValue();
    }

    @Override
    public boolean isValid(Boolean mainValue, ConstraintValidatorContext constraintValidatorContext) {
        switch (this.ruleValue) {
        case NULL:
            return (mainValue == null);
        case NOT_NULL:
            return (mainValue != null);
        case FALSE:
            return (mainValue == false);
        case TRUE:
            return (mainValue == true);
        default:
            return (false);
        }
    }
}
