/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     mx.com.interware.validator.rule
 * Modulo:      Validation
 * Tipo:        RuleUpperCase
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator;

import java.util.Arrays;
import java.util.List;

import mx.com.interware.validator.rule.RuleExtended;
import mx.com.interware.validator.rule.RuleFactory;

/**
 * Permite calcular la fortaleza de la contraseña tras haber ejecutado todas las
 * reglas definidas en la lista de reglas aplicables a un password.
 *
 * @author j49u4r
 *
 */
public final class PasswordStrengthCalculator {

    /**
     * Reglas aplicables a un password. En este caso son las reglas de longitud
     * por defecto(min 4, max 30), 4 puntos y al menos una letra mayúscula, 4.
     * puntos.
     */
    private static final List<RuleExtended> rulesForPassword = Arrays.asList(RuleFactory.ofLengthRule(4),
            RuleFactory.ofRuleUpperCase(4));

    private PasswordStrengthCalculator() {
    }

    /**
     * Ejecuta todas las reglas de validación definidas para un password. Cada
     * regla tiene puntos asignados, mismos que se sumarán a la fortaleza del
     * password si este último cumple dicha regla.
     *
     * @param password
     *            Password sobre el que se ejecutarán las validaciones.
     * @return Puntos que representan la fortaleza de la contraseña.
     */
    public static int calculatePasswordStrength(String password) {
        return rulesForPassword.stream().mapToInt(rv -> rv.validate(password)).sum();
    }
}
