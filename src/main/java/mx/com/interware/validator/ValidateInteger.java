/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidateInteger
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Interfaz de la anotacion ValidateInteger
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Valida atributos o parametros de tipo <code>Integer</code> Es invocada
 * anotando un atributo con <code> @ValidateInteger</code> y puede recibir los
 * siguientes parametros
 * <code>String message, NumberConstraintRule ruleValue, Integer value, Integer leftValue, Integer rightValue</code>
 * <p>
 * Donde:
 * <p>
 * </code>message</code> es el mensaje de retorno cuando la validacion falla.
 * </p>
 * <p>
 * </code>value</code> es el valor con el que se compara el dato anotado.
 * <p>
 * <code>leftValue</code> y <code>rightValue</code> definen el rango de valores
 * en el que se ejecutan operaciones como
 * <code>NumberConstraintRule.BETWEEN_EXCLUSIVE</code>
 * <p>
 * <code>ruleValue</code> es la regla que se utilizara para ejecutar la
 * validacion
 * <p>
 * Ejemplo:
 * <code> @ValidateInteger (ruleValue=NumberConstraintRule.BETWEEN_EXCLUSIVE, value=10, leftValue=0, rightValue=40, message="Dato Invalido")
 * Integer data;
 * </code>
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateIntegerEngine, NumberConstraintRule
 */
@Constraint(validatedBy = { ValidateIntegerEngine.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ValidateInteger {
    String message() default "El tipo Integer no es valido";

    NumberConstraintCode ruleValue() default NumberConstraintCode.ZERO;

    int value() default 0;

    int leftValue() default Integer.MIN_VALUE;

    int rightValue() default Integer.MAX_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
