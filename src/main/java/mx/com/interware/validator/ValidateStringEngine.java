/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     com.sc.support.mail
 * Modulo:      Mail
 * Tipo:        Clase
 * Autor:       Gustavo Adolfo Arellano Sandoval (GAA)
 * Fecha:       Miercoles 01 de Junio de 2016 (21_24)
 * Version:     0.0.1
 * .
 * Cron que se encarga de ejecutar el envio de mails - CronMailService
 *
 * Historia:    .
 *
 *
 */
package mx.com.interware.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class ValidateStringEngine implements ConstraintValidator<ValidateString, String> {
    private ValidatorCode vc;
    private String customRegEx;
    private int minLen;
    private int maxLen;
    private int strength;
    private final String CURP_REGEX = "[A-Z]{4}[0-9]{6}[H,M][A-Z]{4}[0-9]";
    private final String RFC_REGEX = "[A-Z]{4}[0-9]{6}[A-Z0-9]{3}";
    private final String LATIN_WORD_REGEX = "[A-Z a-z]+";
    private final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final String TDC_REGEX = "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6"
            + "(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3" + "(?:0[0-5]|[68][0-9])[0-9]{11}|"
            + "(?:2131|1800|35\\d{3})\\d{11})$";

    @Override
    public void initialize(ValidateString constraintAnnotation) {
        this.vc = constraintAnnotation.value();
        this.customRegEx = constraintAnnotation.customRegEx();
        this.maxLen = constraintAnnotation.maxLen();
        this.minLen = constraintAnnotation.minLen();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isValid(this.vc, value);
    }

    public boolean isValid(ValidatorCode local_vc, String value) {
        switch (local_vc) {
        case PASSWORD_STRENGTH:
            return doValidPass(value);
        case NOT_NULL:
            return (value != null);
        case NOT_EMPTY:
            return (value != null && !value.trim().equals(""));
        case ONLY_LEN:
            return (value.length() >= this.minLen && value.length() <= this.maxLen);
        case PHONE:
            return validPhone(value);
        default:
            Pattern pattern = Pattern.compile(selectRegEx(local_vc));
            Matcher matcher = pattern.matcher(value);
            return matcher.matches();
        }
    }

    private boolean doValidPass(String value) {
        return PasswordStrengthCalculator.calculatePasswordStrength(value) >= this.strength;
    }

    private String selectRegEx(ValidatorCode local_vc) {
        switch (local_vc) {
        case CUSTOM:
            return this.customRegEx;
        case LATIN_WORD:
            return LATIN_WORD_REGEX;
        case EMAIL:
            return EMAIL_REGEX;
        case CURP:
            return CURP_REGEX;
        case RFC:
            return RFC_REGEX;
        case TDC:
            return TDC_REGEX;
        default:
            return "*";
        }
    }

    private boolean validPhone(String phone) {
        int unSoloMas = 0;
        int size = phone.length();
        for (int i = 0; i < size; i++) {
            char vc = phone.charAt(i);
            if (vc == '+') {
                unSoloMas++;
                if (unSoloMas > 1) {
                    return false;
                }
            }
            if (!isCorrect(phone.charAt(i))) {
                return false;
            }
        }
        PhoneNumber number = null;
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            number = phoneUtil.parse(phone, "MX");
        } catch (NumberParseException e) {
            return false;
        }
        if (!phoneUtil.isValidNumber(number)) {
            return false;
        }
        return true;
    }

    private boolean isCorrect(char c) {
        boolean flag = false;
        String valid = "012345 6789()+.-";
        int size = valid.length();
        for (int i = 0; i < size; i++) {
            char vc = valid.charAt(i);
            if (vc == c) {
                flag = true;
            }
        }
        return flag;
    }
}
