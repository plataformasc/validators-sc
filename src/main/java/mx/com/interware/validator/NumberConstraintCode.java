/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        NumberConstraintCode
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016 (21_24)
 * Version:     0.0.1
 * .
 * Contiene las operaciones disponibles para validacion
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

/**
 * Define los codigos de operacion para aplicar en una validacion tipos de dato
 * <code>
 * Integer, Double, Float, Long, Short, Byte, Boolean y Date </code>
 * <p>
 * Los desarrolladores invocan estos codigos al anotar un dato que se pretenda
 * validar
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidateInteger
 */
public enum NumberConstraintCode {
    NULL, 
    NOT_NULL, 
    ZERO, 
    NOT_ZERO, 
    GREATER_THAN, 
    GREATER_THAN_OR_EQUAL_TO, 
    LESS_THAN, 
    LESS_THAN_OR_EQUAL_TO, 
    EQUAL_TO, 
    DIFFERENT_TO, 
    BETWEEN_EXCLUSIVE, 
    BETWEEN_INCLUSIVE, 
    NOT_BETWEEN_EXCLUSIVE, 
    NOT_BETWEEN_INCLUSIVE, 
    NEGATIVE, 
    POSITIVE, 
    FALSE, 
    TRUE
}
