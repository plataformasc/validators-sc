/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     com.sc.support.mail
 * Modulo:      Mail
 * Tipo:        Clase
 * Autor:       Gustavo Adolfo Arellano Sandoval (GAA)
 * Fecha:       Miercoles 01 de Junio de 2016 (21_24)
 * Version:     0.0.1
 * .
 * Cron que se encarga de ejecutar el envio de mails - CronMailService
 *
 * Historia:    .
 *
 *
 */
package mx.com.interware.validator;

/**
 * <p>ValidatorCode class.</p>
 *
 * @author garellano
 * @version $Id: $Id
 */
public enum ValidatorCode {
	// Para Strings
    NOT_NULL,
    NOT_EMPTY, // implica NOT_NULL
    ONLY_LEN, // implica NOT_EMPTY
    EMAIL,
    RFC,
    CURP,
    TDC,
    LATIN_WORD,
    CUSTOM,
    PHONE,
    // Para numeros
    INTEGER,
    PASSWORD_STRENGTH,
    DECIMAL;
}
