/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     mx.com.interware.validator
 * Modulo:      sc
 * Tipo:        ValidatePassword
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Interfaz de la anotacion ValidatePassword
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Valida atributos o parametros de tipo <code>String</code> de tipo Password.
 * Es invocada anotando un atributo con <code> @ValidatePassword</code> y puede
 * recibir los siguientes parametros
 * <code>String message, Integer passwordStrength</code>
 * <p>
 * Donde:
 * <p>
 * </code>message</code> es el mensaje de retorno cuando la validacion falla.
 * </p>
 * <p>
 * </code>passwordStrength</code> es el valor de fortaleza requerido para que el
 * password se considere valido.
 * <p>
 * Ejemplo:
 * <code> @ValidatePassword (passwordStrength=8, message="Dato Invalido")
 * String data;
 * </code>
 *
 * @author RDominguez
 * @version $Id: $Id
 * @see ValidatePasswordEngine
 */
@Constraint(validatedBy = { ValidatePasswordEngine.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ValidatePassword {
    String message() default "El Password no es valido";

    int passwordStrength() default 4;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
