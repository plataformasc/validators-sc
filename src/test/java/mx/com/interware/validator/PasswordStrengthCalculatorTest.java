/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              InterWare de México SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    traine
 * Paquete:     com.sc.support.validator.rule
 * Modulo:      Validation
 * Tipo:        PasswordStrengthCalculatorTest
 * Autor:       j49u4r
 * Fecha:       Jul 18, 2016
 * Version:
 *
 * Historia:    .
 *              Jul 18, 2016 Generado por j49u4r
 */
package mx.com.interware.validator;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Clase de pruebas unitarias del componente de validación de reglas aplicables
 * sobre un password.
 *
 * @author j49u4r
 *
 */
public class PasswordStrengthCalculatorTest {

    private String passwordWithStrength0;
    private String passwordWithStrength4;
    private String passwordWithStrength8;
    private PasswordStrengthCalculator psc;

    /**
     * Se definen los datos de pruebas sobre los que se ejecutarán las
     * validaciones. En este caso serán los passwords y la instancia del
     * calculador de fortaleza.
     */
    @Before
    public void init() {

	// La regla especifíca que la longitud mínima es 4
	// y la máxima es 30. Esta regla tiene un valor de 4 puntos
	passwordWithStrength0 = "1w3";
	passwordWithStrength4 = "1w3zr";
	passwordWithStrength8 = "1w3zrPWX4";
    }

    /**
     * Los puntos obtenidos tras la ejecución de las reglas sobre
     * passwordWithStrength0 dará cero puntos puesto que no cumple ninguna.
     */
    @Test
    public void calculatePasswordStrength0Test() {
	assertEquals(0, psc.calculatePasswordStrength(passwordWithStrength0));
    }

    /**
     * Los puntos obtenidos tras la ejecución de las reglas sobre
     * passwordWithStrength4 dará cuatro puntos puesto que cumple solo una regla
     * de 4 puntos.
     */
    @Test
    public void calculatePasswordStrength4Test() {
	assertEquals(4, psc.calculatePasswordStrength(passwordWithStrength4));
    }

    /**
     * Los puntos obtenidos tras la ejecución de las reglas sobre
     * passwordWithStrength8 dará ocho puntos puesto que cumple dos reglas de 4
     * puntos cada una.
     */
    @Test
    public void calculatePasswordStrength8Test() {
	assertEquals(8, psc.calculatePasswordStrength(passwordWithStrength8));
    }
}
