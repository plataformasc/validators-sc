package mx.com.interware.validator;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class ValidatorTest {
	private static ValidatorFactory vf;
	private static Validator validator;

	/**
	 * Setting up the validator and model data.
	 */
	@BeforeClass
	public static void setup() {
		vf = Validation.buildDefaultValidatorFactory();
		validator = vf.getValidator();

	}

	/**
	 * Validating the model data which has correct values.
	 */
	@Test
	public void testCorrectAddress() {
		System.out.println("********** Running validator with corret address **********");

		Set<ConstraintViolation<String>> violations = validator.validate("AESG");

		assertEquals(violations.size(), 0);
	}

	/**
	 * Validating the model data which has incorrect values.
	 */
	@Test
	public void testInvalidAddress() {
		System.out.println("********** Running validator against wrong address **********");

		// setting address itself as null
		// person.setAddress(null);
		//validateAndPrintErrors("sandoval");

		// Pruebas positivas
		ValidateStringEngine vse = new ValidateStringEngine();
		assertTrue(vse.isValid(ValidatorCode.CURP, "AESG671022HASRN2"));
		//assertTrue(vse.isValid(ValidatorCode.CUSTOM, ""));
		assertTrue(vse.isValid(ValidatorCode.EMAIL, "gus@aol.com"));
		assertTrue(vse.isValid(ValidatorCode.LATIN_WORD, "gustavo es un nino"));
		assertTrue(vse.isValid(ValidatorCode.NOT_EMPTY, "Hola"));
		assertTrue(vse.isValid(ValidatorCode.NOT_NULL, "OK"));
		//assertTrue(vse.isValid(ValidatorCode.ONLY_LEN, ""));
		assertTrue(vse.isValid(ValidatorCode.PHONE, "+52 55 2448 3672"));
		assertTrue(vse.isValid(ValidatorCode.RFC, "AESG671022IZ7"));
		assertTrue(vse.isValid(ValidatorCode.TDC, "5491380207903333"));
		
		// Pruebas Negativas
		assertFalse(vse.isValid(ValidatorCode.CURP, ""));
		//assertFalse(vse.isValid(ValidatorCode.CUSTOM, "//d"));
		assertFalse(vse.isValid(ValidatorCode.EMAIL, "x"));
		assertFalse(vse.isValid(ValidatorCode.LATIN_WORD, "ç"));
		assertFalse(vse.isValid(ValidatorCode.NOT_EMPTY, ""));
		assertFalse(vse.isValid(ValidatorCode.NOT_NULL, null));
		//assertFalse(vse.isValid(ValidatorCode.ONLY_LEN, ""));
		assertFalse(vse.isValid(ValidatorCode.PHONE, "abc"));
		assertFalse(vse.isValid(ValidatorCode.RFC, "xyz"));
		assertFalse(vse.isValid(ValidatorCode.TDC, "rst"));
	}

	private void validateAndPrintErrors(String aValidar) {
		Set<ConstraintViolation<String>> violations = validator.validate(aValidar);

		for (ConstraintViolation<String> viol : violations) {
			System.out.println(viol.getMessage());
		}
		assertEquals(1, violations.size());
	}
	
}
