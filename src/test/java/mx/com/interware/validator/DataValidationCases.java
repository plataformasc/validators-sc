/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     com.sc.support.validator
 * Modulo:      sc
 * Tipo:        DataValidationCases
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que contiene los casos para validacion de anotaciones
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.io.Serializable;
import java.util.Date;

/**
 * Lista los los casos en los que se usan las anotaciones de validacion
 * 
 * @author rafael.dominguez
 */
public class DataValidationCases implements Serializable {

    private static final long serialVersionUID = -7045568421223949109L;

    @ValidateDate(ruleValue = NumberConstraintCode.GREATER_THAN, value = "2012-04-01")
    private Date today;

    @ValidatePassword(passwordStrength = 8)
    String passwordStrength1 = "1w3zrPWX4";

    @ValidatePassword(passwordStrength = 8)
    String passwordStrength3 = "1w3";

    @ValidatePassword(passwordStrength = 1)
    String passwordStrength4 = "1w32";

    @ValidateBoolean(ruleValue = NumberConstraintCode.NULL)
    private Boolean correctBooleanData00 = null;

    @ValidateBoolean(ruleValue = NumberConstraintCode.NOT_NULL)
    private boolean correctBooleanData01 = false;

    @ValidateBoolean(ruleValue = NumberConstraintCode.FALSE)
    private boolean correctBooleanData02 = false;

    @ValidateBoolean(ruleValue = NumberConstraintCode.TRUE)
    private boolean correctBooleanData03 = true;

    @ValidateBoolean(ruleValue = NumberConstraintCode.NULL)
    private Boolean correctBooleanData04 = false;

    @ValidateBoolean(ruleValue = NumberConstraintCode.NOT_NULL)
    private Boolean correctBooleanData05 = null;

    @ValidateBoolean(ruleValue = NumberConstraintCode.FALSE)
    private boolean correctBooleanData06 = true;

    @ValidateBoolean(ruleValue = NumberConstraintCode.TRUE)
    private boolean correctBooleanData07 = false;

    @ValidateByte(ruleValue = NumberConstraintCode.NULL)
    private Byte correctByteData00 = null;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_NULL)
    private Byte correctByteData01 = 7;

    @ValidateByte(ruleValue = NumberConstraintCode.ZERO)
    private Byte correctByteData02 = 0;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Byte correctByteData03 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.GREATER_THAN, value = Byte.MIN_VALUE)
    private Byte correctByteData04 = 0;

    @ValidateByte(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Byte.MIN_VALUE)
    private Byte correctByteData05 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.LESS_THAN, value = Byte.MAX_VALUE)
    private Byte correctByteData06 = Byte.MAX_VALUE - 1;

    @ValidateByte(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Byte.MAX_VALUE)
    private Byte correctByteData07 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.EQUAL_TO, value = Byte.MAX_VALUE)
    private Byte correctByteData08 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Byte.MAX_VALUE)
    private Byte correctByteData09 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = Byte.MIN_VALUE, rightValue = Byte.MAX_VALUE)
    private Byte correctByteData10 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = Byte.MIN_VALUE, rightValue = Byte.MAX_VALUE)
    private Byte correctByteData11 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData12 = 10;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData13 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.NEGATIVE)
    private Byte correctByteData14 = -20;

    @ValidateByte(ruleValue = NumberConstraintCode.POSITIVE)
    private Byte correctByteData15 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.NULL)
    private Byte correctByteData16 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_NULL)
    private Byte correctByteData17 = null;

    @ValidateByte(ruleValue = NumberConstraintCode.ZERO)
    private Byte correctByteData18 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Byte correctByteData19 = 0;

    @ValidateByte(ruleValue = NumberConstraintCode.GREATER_THAN, value = Byte.MIN_VALUE)
    private Byte correctByteData20 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Byte.MAX_VALUE)
    private Byte correctByteData21 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.LESS_THAN, value = Byte.MIN_VALUE)
    private Byte correctByteData22 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Byte.MIN_VALUE)
    private Byte correctByteData23 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.EQUAL_TO, value = Byte.MAX_VALUE)
    private Byte correctByteData24 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Byte.MIN_VALUE)
    private Byte correctByteData25 = Byte.MIN_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData26 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData27 = Byte.MAX_VALUE;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData29 = 5;

    @ValidateByte(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData30 = 10;

    @ValidateByte(ruleValue = NumberConstraintCode.NEGATIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData31 = 20;

    @ValidateByte(ruleValue = NumberConstraintCode.POSITIVE, leftValue = 0, rightValue = 10)
    private Byte correctByteData32 = -20;

    @ValidateInteger(ruleValue = NumberConstraintCode.NULL)
    private Integer correctIntegerData00 = null;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_NULL)
    private Integer correctIntegerData01 = 20;

    @ValidateInteger(ruleValue = NumberConstraintCode.ZERO)
    private Integer correctIntegerData02 = 0;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Integer correctIntegerData03 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10)
    private Integer correctIntegerData04 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10)
    private Integer correctIntegerData05 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.LESS_THAN, value = 10)
    private Integer correctIntegerData06 = 9;

    @ValidateInteger(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10)
    private Integer correctIntegerData07 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10)
    private Integer correctIntegerData08 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10)
    private Integer correctIntegerData09 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData10 = 3;

    @ValidateInteger(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData11 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData12 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData13 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.NEGATIVE)
    private Integer correctIntegerData14 = -20;

    @ValidateInteger(ruleValue = NumberConstraintCode.POSITIVE)
    private Integer correctIntegerData32 = 20;

    @ValidateInteger(ruleValue = NumberConstraintCode.NULL)
    private Integer correctIntegerData15 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_NULL)
    private Integer correctIntegerData16 = null;

    @ValidateInteger(ruleValue = NumberConstraintCode.ZERO)
    private Integer correctIntegerData17 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Integer correctIntegerData18 = 0;

    @ValidateInteger(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10)
    private Integer correctIntegerData19 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10)
    private Integer correctIntegerData20 = 9;

    @ValidateInteger(ruleValue = NumberConstraintCode.LESS_THAN, value = 10)
    private Integer correctIntegerData21 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10)
    private Integer correctIntegerData22 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10)
    private Integer correctIntegerData23 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10)
    private Integer correctIntegerData24 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData25 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData26 = 11;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData27 = 9;

    @ValidateInteger(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Integer correctIntegerData29 = 10;

    @ValidateInteger(ruleValue = NumberConstraintCode.NEGATIVE)
    private Integer correctIntegerData30 = 30;

    @ValidateInteger(ruleValue = NumberConstraintCode.POSITIVE)
    private Integer correctIntegerData31 = -10;

    @ValidateDouble(ruleValue = NumberConstraintCode.NULL)
    private Double correctDoubleData00 = null;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_NULL)
    private Double correctDoubleData01 = 20.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.ZERO)
    private Double correctDoubleData02 = 0.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Double correctDoubleData03 = 10.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10)
    private Double correctDoubleData04 = 11.7;

    @ValidateDouble(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10.6)
    private Double correctDoubleData05 = 10.6;

    @ValidateDouble(ruleValue = NumberConstraintCode.LESS_THAN, value = 10.0)
    private Double correctDoubleData06 = 9.9;

    @ValidateDouble(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10.0)
    private Double correctDoubleData07 = 10.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10.0)
    private Double correctDoubleData08 = 10.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10.0)
    private Double correctDoubleData09 = 11.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0.0, rightValue = 10.0)
    private Double correctDoubleData10 = 3.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0.8, rightValue = 10.5)
    private Double correctDoubleData11 = 10.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10.5)
    private Double correctDoubleData12 = 10.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0.5, rightValue = 10.5)
    private Double correctDoubleData13 = 11.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.NEGATIVE)
    private Double correctDoubleData14 = -20.7;

    @ValidateDouble(ruleValue = NumberConstraintCode.POSITIVE)
    private Double correctDoubleData32 = 20.8;

    @ValidateDouble(ruleValue = NumberConstraintCode.NULL)
    private Double correctDoubleData15 = 10.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_NULL)
    private Double correctDoubleData16 = null;

    @ValidateDouble(ruleValue = NumberConstraintCode.ZERO)
    private Double correctDoubleData17 = 10.9;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Double correctDoubleData18 = 0.0;

    @ValidateDouble(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10.4)
    private Double correctDoubleData19 = 10.4;

    @ValidateDouble(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10.6)
    private Double correctDoubleData20 = 9.6;

    @ValidateDouble(ruleValue = NumberConstraintCode.LESS_THAN, value = 10.5)
    private Double correctDoubleData21 = 11.9;

    @ValidateDouble(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10.1)
    private Double correctDoubleData22 = 11.1;

    @ValidateDouble(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10.0)
    private Double correctDoubleData23 = 3.2;

    @ValidateDouble(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10.4)
    private Double correctDoubleData24 = 10.4;

    @ValidateDouble(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0.1, rightValue = 10.3)
    private Double correctDoubleData25 = 10.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0.2, rightValue = 10.1)
    private Double correctDoubleData26 = 40.43;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0.0, rightValue = 10.4)
    private Double correctDoubleData27 = 5.5;

    @ValidateDouble(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0.0, rightValue = 10.4)
    private Double correctDoubleData29 = 10.4;

    @ValidateDouble(ruleValue = NumberConstraintCode.NEGATIVE)
    private Double correctDoubleData30 = 30.432;

    @ValidateDouble(ruleValue = NumberConstraintCode.POSITIVE)
    private Double correctDoubleData31 = -10.123;

    @ValidateFloat(ruleValue = NumberConstraintCode.NULL)
    private Float correctFloatData00 = null;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_NULL)
    private Float correctFloatData01 = 20.4f;

    @ValidateFloat(ruleValue = NumberConstraintCode.ZERO)
    private Float correctFloatData02 = 0.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Float correctFloatData03 = 10.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10f)
    private Float correctFloatData04 = 11.7f;

    @ValidateFloat(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10.6f)
    private Float correctFloatData05 = 10.6f;

    @ValidateFloat(ruleValue = NumberConstraintCode.LESS_THAN, value = 10.0f)
    private Float correctFloatData06 = 9.9f;

    @ValidateFloat(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10.0f)
    private Float correctFloatData07 = 10.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10.0f)
    private Float correctFloatData08 = 10.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10.0f)
    private Float correctFloatData09 = 11.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0.0f, rightValue = 10.0f)
    private Float correctFloatData10 = 3.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0.8f, rightValue = 10.5f)
    private Float correctFloatData11 = 10.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0f, rightValue = 10.5f)
    private Float correctFloatData12 = 10.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0.5f, rightValue = 10.5f)
    private Float correctFloatData13 = 11.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NEGATIVE)
    private Float correctFloatData14 = -20.7f;

    @ValidateFloat(ruleValue = NumberConstraintCode.POSITIVE)
    private Float correctFloatData32 = 20.8f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NULL)
    private Float correctFloatData15 = 10.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_NULL)
    private Float correctFloatData16 = null;

    @ValidateFloat(ruleValue = NumberConstraintCode.ZERO)
    private Float correctFloatData17 = 10.9f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Float correctFloatData18 = 0.0f;

    @ValidateFloat(ruleValue = NumberConstraintCode.GREATER_THAN, value = 10.4f)
    private Float correctFloatData19 = 10.4f;

    @ValidateFloat(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = 10.6f)
    private Float correctFloatData20 = 9.6f;

    @ValidateFloat(ruleValue = NumberConstraintCode.LESS_THAN, value = 10.5f)
    private Float correctFloatData21 = 11.9f;

    @ValidateFloat(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = 10.1f)
    private Float correctFloatData22 = 11.1f;

    @ValidateFloat(ruleValue = NumberConstraintCode.EQUAL_TO, value = 10.0f)
    private Float correctFloatData23 = 3.2f;

    @ValidateFloat(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = 10.4f)
    private Float correctFloatData24 = 10.4f;

    @ValidateFloat(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0.1f, rightValue = 10.3f)
    private Float correctFloatData25 = 10.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0.2f, rightValue = 10.1f)
    private Float correctFloatData26 = 40.43f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0.0f, rightValue = 10.4f)
    private Float correctFloatData27 = 5.5f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0.0f, rightValue = 10.4f)
    private Float correctFloatData29 = 10.4f;

    @ValidateFloat(ruleValue = NumberConstraintCode.NEGATIVE)
    private Float correctFloatData30 = 30.432f;

    @ValidateFloat(ruleValue = NumberConstraintCode.POSITIVE)
    private Float correctFloatData31 = -10.123f;

    @ValidateLong(ruleValue = NumberConstraintCode.NULL)
    private Long correctLongData00 = null;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_NULL)
    private Long correctLongData01 = 7372036854775807L;

    @ValidateLong(ruleValue = NumberConstraintCode.ZERO)
    private Long correctLongData02 = 0L;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Long correctLongData03 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.GREATER_THAN, value = Long.MIN_VALUE)
    private Long correctLongData04 = 7381739213L;

    @ValidateLong(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Long.MIN_VALUE)
    private Long correctLongData05 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.LESS_THAN, value = Long.MAX_VALUE)
    private Long correctLongData06 = Long.MAX_VALUE - 1;

    @ValidateLong(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Long.MAX_VALUE)
    private Long correctLongData07 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.EQUAL_TO, value = Long.MAX_VALUE)
    private Long correctLongData08 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Long.MAX_VALUE)
    private Long correctLongData09 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = Long.MIN_VALUE, rightValue = Long.MAX_VALUE)
    private Long correctLongData10 = 2048237L;

    @ValidateLong(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = Long.MIN_VALUE, rightValue = Long.MAX_VALUE)
    private Long correctLongData11 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData12 = 10L;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData13 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.NEGATIVE)
    private Long correctLongData14 = -20L;

    @ValidateLong(ruleValue = NumberConstraintCode.POSITIVE)
    private Long correctLongData15 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.NULL)
    private Long correctLongData16 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_NULL)
    private Long correctLongData17 = null;

    @ValidateLong(ruleValue = NumberConstraintCode.ZERO)
    private Long correctLongData18 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Long correctLongData19 = 0L;

    @ValidateLong(ruleValue = NumberConstraintCode.GREATER_THAN, value = Long.MIN_VALUE)
    private Long correctLongData20 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Long.MAX_VALUE)
    private Long correctLongData21 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.LESS_THAN, value = Long.MIN_VALUE)
    private Long correctLongData22 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Long.MIN_VALUE)
    private Long correctLongData23 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.EQUAL_TO, value = Long.MAX_VALUE)
    private Long correctLongData24 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Long.MIN_VALUE)
    private Long correctLongData25 = Long.MIN_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData26 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData27 = Long.MAX_VALUE;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData29 = 5L;

    @ValidateLong(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0L, rightValue = 10L)
    private Long correctLongData30 = 10L;

    @ValidateLong(ruleValue = NumberConstraintCode.NEGATIVE, leftValue = 0, rightValue = 10L)
    private Long correctLongData31 = 20L;

    @ValidateLong(ruleValue = NumberConstraintCode.POSITIVE, leftValue = 0, rightValue = 10L)
    private Long correctLongData32 = -20L;

    @ValidateShort(ruleValue = NumberConstraintCode.NULL)
    private Short correctShortData00 = null;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_NULL)
    private Short correctShortData01 = 7;

    @ValidateShort(ruleValue = NumberConstraintCode.ZERO)
    private Short correctShortData02 = 0;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Short correctShortData03 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.GREATER_THAN, value = Short.MIN_VALUE)
    private Short correctShortData04 = 0;

    @ValidateShort(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Short.MIN_VALUE)
    private Short correctShortData05 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.LESS_THAN, value = Short.MAX_VALUE)
    private Short correctShortData06 = Short.MAX_VALUE - 1;

    @ValidateShort(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Short.MAX_VALUE)
    private Short correctShortData07 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.EQUAL_TO, value = Short.MAX_VALUE)
    private Short correctShortData08 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Short.MAX_VALUE)
    private Short correctShortData09 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = Short.MIN_VALUE, rightValue = Short.MAX_VALUE)
    private Short correctShortData10 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = Short.MIN_VALUE, rightValue = Short.MAX_VALUE)
    private Short correctShortData11 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData12 = 10;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData13 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.NEGATIVE)
    private Short correctShortData14 = -20;

    @ValidateShort(ruleValue = NumberConstraintCode.POSITIVE)
    private Short correctShortData15 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.NULL)
    private Short correctShortData16 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_NULL)
    private Short correctShortData17 = null;

    @ValidateShort(ruleValue = NumberConstraintCode.ZERO)
    private Short correctShortData18 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_ZERO)
    private Short correctShortData19 = 0;

    @ValidateShort(ruleValue = NumberConstraintCode.GREATER_THAN, value = Short.MIN_VALUE)
    private Short correctShortData20 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.GREATER_THAN_OR_EQUAL_TO, value = Short.MAX_VALUE)
    private Short correctShortData21 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.LESS_THAN, value = Short.MIN_VALUE)
    private Short correctShortData22 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.LESS_THAN_OR_EQUAL_TO, value = Short.MIN_VALUE)
    private Short correctShortData23 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.EQUAL_TO, value = Short.MAX_VALUE)
    private Short correctShortData24 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.DIFFERENT_TO, value = Short.MIN_VALUE)
    private Short correctShortData25 = Short.MIN_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData26 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData27 = Short.MAX_VALUE;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_BETWEEN_EXCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData29 = 5;

    @ValidateShort(ruleValue = NumberConstraintCode.NOT_BETWEEN_INCLUSIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData30 = 10;

    @ValidateShort(ruleValue = NumberConstraintCode.NEGATIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData31 = 20;

    @ValidateShort(ruleValue = NumberConstraintCode.POSITIVE, leftValue = 0, rightValue = 10)
    private Short correctShortData32 = -20;

}
