/*
 * Licencia:    Este código se encuentra bajo la protección
 *              que otorga el contrato establecido entre
 *              Interware SA de CV y su cliente, Kuspit por lo
 *              que queda estrictamente prohibido copiar, donar
 *              vender y/o distribuir el presente código por
 *              cualquier medio electrónico o impreso sin el
 *              permiso explícito y por escrito del cliente.
 *
 * Proyecto:    SaludCercana
 * Paquete:     com.sc.support.validator
 * Modulo:      sc
 * Tipo:        DataValidationCases
 * Autor:       Rafael Dominguez Lazaro (RDominguez)
 * Fecha:       Viernes 22 de Julio de 2016
 * Version:     0.0.1
 * .
 * Clase que contiene los casos para validacion de anotaciones
 *
 * Historia:
 *
 *
 */
package mx.com.interware.validator;

import java.util.Set;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ConstraintViolation;
import org.junit.Test;
import org.junit.Before;

/**
 * Ejecuta las pruebas sobre los objetos de validacion contenidos en DataValidationCases
 * 
 * @author rafael.dominguez
 */
public class AnnotatedValidatorsTest {

    private Validator validator;
    private DataValidationCases validationCases; 

    @Before
    public void initObjects() {
        validationCases = new DataValidationCases();
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void prueba() {
        Set<ConstraintViolation<DataValidationCases>> constraintViolations = validator.validate(validationCases);
        for (ConstraintViolation<DataValidationCases> violations : constraintViolations) {
            System.out.println(violations.getMessage() + " " + violations.getPropertyPath().toString());
        }

    }
}
